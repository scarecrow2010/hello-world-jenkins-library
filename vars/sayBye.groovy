def call(Map pipelineParams) {
    
   pipeline {
        agent any
        environment {
            EMAIL_NOTIFICATION = "a@a.com"
        }
        stages {
            stage('Greetings') {
                steps {
                   script {
                        def greetings = "Name is mandatory"

                        if ("${pipelineParams.name}" != "null" && "${pipelineParams.name}") 
                        {
                            greetings = "$pipelineParams.name"
                        } 

                        echo "$greetings"
                   }
                }
            }
            stage('Test') {
                steps {
                    echo 'Testing..'
                }
            }
            stage('Deploy') {
                steps {
                    echo 'Deploying....'
                }
            }
        }
        post {
            
            success {
                echo 'success..'
                echo 'email: ${env.EMAIL_NOTIFICATION}'
                echo '$env.EMAIL_NOTIFICATION'
                echo "$pipelineParams.name"
            }
            failure {
                echo 'failure..'
            }
        }
    }
}
